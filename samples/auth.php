<?php

require_once __DIR__ . '/../vendor/autoload.php';


# Instance SV client get auth automatically
$client = new \Kronoapp\SVL\SVLClient('chile', 'config');

# check you access_token
# Note: Access token is protected property read only
echo '<pre>';
print_r($client);
echo '</pre>';
