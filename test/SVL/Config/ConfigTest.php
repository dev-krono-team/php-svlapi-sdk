<?php


namespace Kronoapp\Test\SVL\Config;


use Kronoapp\SVL\Config\Config;
use Kronoapp\SVL\Exception\SVLException;
use PHPUnit\Framework\TestCase;

class ConfigTest extends TestCase
{

    public function testLoadConfig()
    {
        Config::loadConfig($site = 'country', $file = 'config.example');
        $this->assertArrayHasKey('datetime_zone', Config::all());
        $this->assertArrayHasKey('client_id', Config::all());
        $this->assertArrayHasKey('client_secret', Config::all());
    }

    public function testLoadConfigException()
    {
        $this->expectException(SVLException::class);
        Config::loadConfig($site = 'country', 'config.ex');
    }

    public function testIsDebug()
    {
        $this->assertFalse(Config::debug());
    }


}