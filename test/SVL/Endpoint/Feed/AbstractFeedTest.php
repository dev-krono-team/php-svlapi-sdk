<?php

namespace Kronoapp\Test\SVL\Endpoint\Feed;

use Kronoapp\SVL\Endpoint\Feed\AbstractFeed;
use PHPUnit\Framework\TestCase;

class AbstractFeedTest extends TestCase
{
    public function testFeedTypes()
    {
        $this->assertEquals('PRICE', AbstractFeed::PRICE);
        $this->assertEquals('INVENTORY', AbstractFeed::INVENTORY);
        $this->assertEquals('SALES_SCHEDULE', AbstractFeed::SALES_SCHEDULE);
        $this->assertEquals('SALES_CANCEL', AbstractFeed::SALES_CANCEL);
        $this->assertEquals('ORDERS_CANCEL', AbstractFeed::ORDERS_CANCEL);
        $this->assertEquals('ORDERS_RESCHEDULE', AbstractFeed::ORDERS_RESCHEDULE);
        $this->assertEquals('ORDERS_RESCHEDULE', AbstractFeed::ORDERS_RESCHEDULE);
        $this->assertEquals('ORDERS_CONFIRM', AbstractFeed::ORDERS_CONFIRM);
    }

}
