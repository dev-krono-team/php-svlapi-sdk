<?php
declare(strict_types=1);

namespace Kronoapp\SVL;


use Kronoapp\SVL\Exception\SVLException;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Client;
use Kronoapp\SVL\Config\Config;

class SVLClient
{

    const BASE_URI = 'https://gwsellerlink.falabella.com';
    const BASE_URI_DEV = 'https://dev-gwsellerlink.falabella.com';
    const VERSION = '1.0';

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var string
     */
    protected $token;

    /**
     * SVLClient constructor.
     * @param string $site
     * @param string $file
     * @param ClientInterface|null $client
     * @throws SVLException
     */
    public function __construct(string $site, string $file, ClientInterface $client = null)
    {
        # Load configuration file
        Config::loadConfig($site, $file);
        # Set HTTP client
        if (!is_null($client)) {
            $this->client = $client;
        } else {
            $this->client = new Client([
                'verify' => false,
                'version' => self::VERSION
            ]);
        }
        # Authenticate and retrieve token client
        if (is_null($this->token)) {
            $this->auth();
        }
    }

    /**
     * Request token
     *
     * @throws SVLException
     */
    protected function auth(): void
    {
        $options = [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => Config::all()['client_id'],
                'client_secret' => Config::all()['client_secret']
            ],
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
        ];
        $response = $this->sendRequest(
            'POST',
            'auth/token',
            $options
        );
        $this->setToken($response->access_token);
    }

    /**
     * @param string $resource
     * @param array $query
     * @return mixed|object
     * @throws SVLException
     */
    public function get(string $resource, array $query = [])
    {
        return $this->sendRequest('GET', $resource, [
            'query' => $query
        ]);
    }

    /**
     * @param string $resource
     * @param array $data
     * @param array $query
     * @return mixed|object
     * @throws SVLException
     */
    public function post(string $resource, array $data, array $query = [])
    {
        return $this->sendRequest('POST', $resource, [
            'json' => $data,
            'query' => $query
        ]);
    }

    /**
     * @param string $method
     * @param string $resource
     * @param array $options
     * @return object|mixed
     * @throws SVLException
     */
    protected function sendRequest(string $method, string $resource, array $options = [])
    {
        $uri = sprintf('%s/%s',
            Config::debug() ? self::BASE_URI_DEV : self::BASE_URI,
            $resource
        );
        $headers = [
            'Content-Type' => 'Application/json'
        ];
        if (!is_null($this->token)) {
            $headers['Authorization'] = 'Bearer ' . $this->token;
        }
        $request = new Request($method, $uri, $headers);
        try {
            $response = $this->client->send($request, $options);
        } catch (GuzzleException $ex) {
            throw new SVLException($ex->getMessage(), $ex->getCode());
        }
        return $this->parseResponse($response);
    }

    /**
     * Set token client
     *
     * @param string $token
     */
    protected function setToken(string $token): void
    {
        $this->token = $token;
    }

    protected function parseResponse(ResponseInterface $response)
    {
        return json_decode($response->getBody()->getContents());
    }
}