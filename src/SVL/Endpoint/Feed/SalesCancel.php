<?php


namespace Kronoapp\SVL\Endpoint\Feed;


use Kronoapp\SVL\Exception\SVLException;

class SalesCancel extends AbstractFeed
{
    /**
     * @var mixed
     */
    private $sku;

    /**
     * Create a
     *
     * @throws SVLException
     */
    public function create()
    {
        return $this->getHTTPClient()->post(
    self::RESOURCE,
            $this->getData()
        );
    }

    /**
     * {@inheritDoc}
     */
    public function setData(array $data = null): void
    {
        parent::setData(
            [
                'type' => self::SALES_CANCEL,
                'data' => is_null($data) ? $this->getSku():$data
            ]
        );
    }

    public function setSku($sku): void
    {
        if (is_array($sku)) {
            $this->sku = $sku;
        } else {
            $this->sku = [['sku' => $sku]];
        }
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

}