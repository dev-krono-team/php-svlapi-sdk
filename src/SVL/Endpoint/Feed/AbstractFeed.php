<?php
declare(strict_types=1);

namespace Kronoapp\SVL\Endpoint\Feed;


use Kronoapp\SVL\Contracts\FeedInterface;
use Kronoapp\SVL\Exception\SVLException;
use Kronoapp\SVL\SVLClient;

abstract class AbstractFeed implements FeedInterface
{

    const RESOURCE = 'feeds';

    /**
     * @var array
     */
    private $data;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $uuid;

    /**
     * @var SVLClient
     */
    private $HTTPClient;

    public function __construct(SVLClient $HTTPClient)
    {
        $this->HTTPClient = $HTTPClient;
    }

    /**
     * Create a feed resource
     *
     * @return mixed
     */
    abstract public function create();

    /**
     * Returns a feed source by UUID
     *
     * @return mixed
     */
    /**
     * @throws SVLException
     */
    public function get()
    {
        return $this->getHTTPClient()->get(self::RESOURCE);
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return SVLClient
     */
    public function getHTTPClient(): SVLClient
    {
        return $this->HTTPClient;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid(string $uuid): void
    {
        $this->uuid = $uuid;
    }

}