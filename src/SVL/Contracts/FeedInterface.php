<?php


namespace Kronoapp\SVL\Contracts;


interface FeedInterface
{
    const PRICE = 'PRICE';
    const INVENTORY = 'INVENTORY';
    const SALES_SCHEDULE = 'SALES_SCHEDULE';
    const SALES_CANCEL = 'SALES_CANCEL';
    const ORDERS_CANCEL = 'ORDERS_CANCEL';
    const ORDERS_RESCHEDULE = 'ORDERS_RESCHEDULE';
    const ORDERS_CONFIRM = 'ORDERS_CONFIRM';
    const INVALID_BODY = 400;
    const NO_DATA = 400;
    const DATA_NOT_ARRAY = 400;
    const CONTENT_IS_GREATER = 400;
    const INVALID_ATTRIBUTE = 400;
    const INVALID_DATA = 400;
    const FEED_TYPE_BLOCKED = 422;

    public function setData(array $data);
    public function getData(): array;
}